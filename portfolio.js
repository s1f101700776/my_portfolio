var request = new XMLHttpRequest();
var test;

function onButtonClick() {
  check_Tokyo = document.place.Tokyo.checked;
  check_Saitama = document.place.Saitama.checked;
  check_Kanagawa = document.place.Kanagawa.checked;

  var Tokyo_temp = document.getElementById("Tokyo_temp");
  var Saitama_temp = document.getElementById("Saitama_temp");
  var Kanagawa_temp = document.getElementById("Kanagawa_temp");

  if (check_Tokyo == true) {
    request.open('GET', 'https://api.openweathermap.org/data/2.5/weather?q=Tokyo,jp&units=metric&appid=2a4c511e0d0a4300503f0fc606f7a363', true);
    request.responseType = 'json';

    request.onload = function () {
      data = this.response;
      main = data.main;
      temp = Math.round(main.temp);
      temp_max = Math.round(main.temp_max);
      temp_min = Math.round(main.temp_min);
      pre = main.pressure;
      console.log(data);

      console.log(main);
      Tokyo_temp.innerHTML = "<b>" + "東京都" + "</b>" + "<br/>" +
                             "今の気温:" + temp + "℃" + "<br/>" +
                             "今の気圧:" + pre + "hPa" + "<br/>" +
                             "最高気温:" + temp_max + "℃" +
                             "最低気温:" + temp_min + "℃" + "<br/>";
      document.getElementById("Tokyo_temp").style.display="block";
    };
    request.send();
  }
  else {
    document.getElementById("Tokyo_temp").style.display="none";
  }


  if (check_Saitama == true) {
    request.open('GET', 'https://api.openweathermap.org/data/2.5/weather?q=Saitama,jp&units=metric&appid=2a4c511e0d0a4300503f0fc606f7a363', true);
    request.responseType = 'json';

    request.onload = function () {
      data = this.response;
      main = data.main;
      temp = Math.round(main.temp);
      temp_max = Math.round(main.temp_max);
      temp_min = Math.round(main.temp_min);
      pre = main.pressure;

      console.log(main);
      Saitama_temp.innerHTML =   "<b>" + "埼玉県" + "</b>" + "<br/>" +
                                 "今の気温:" + temp + "℃" + "<br/>" +
                                 "今の気圧:" + pre + "hPa" + "<br/>" +
                                 "最高気温:" + temp_max + "℃" + "<br/>" +
                                 "最低気温:" + temp_min + "℃" + "<br/>";
      document.getElementById("Saitama_temp").style.display="block";
    };
    request.send();
  }
  else {
    document.getElementById("Saitama_temp").style.display="none";
  }

  if (check_Kanagawa == true) {
    request.open('GET', 'https://api.openweathermap.org/data/2.5/weather?q=Kanagawa,jp&units=metric&appid=2a4c511e0d0a4300503f0fc606f7a363', true);
    request.responseType = 'json';

    request.onload = function () {
      data = this.response;
      main = data.main;
      temp = Math.round(main.temp);
      temp_max = Math.round(main.temp_max);
      temp_min = Math.round(main.temp_min);
      pre = main.pressure;

      console.log(main);
      Kanagawa_temp.innerHTML =  "<b>" + "神奈川県" + "</b>" + "<br/>" +
                                 "今の気温:" + temp + "℃" + "<br/>" +
                                 "今の気圧:" + pre + "hPa" + "<br/>" +
                                 "最高気温:" + temp_max + "℃" + "<br/>" +
                                 "最低気温:" + temp_min + "℃" + "<br/>";
      document.getElementById("Kanagawa_temp").style.display="block";
    };
    request.send();
  }
  else {
    document.getElementById("Kanagawa_temp").style.display="none";
  }
}
